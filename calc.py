
def suma (a: int, b: int):
    return a+b

def resta(a: int, b: int):
    return b-a


if __name__ == "__main__":
    x1 = suma(1,2)
    x2 = suma(3,4)
    x3 = resta(5,6)
    x4 = resta(7,9)
    print(f"La suma de 1 y 2 es {x1}")
    print(f"La suma de 3 y 4 es {x2}")
    print(f"La resta de 5 y 6 es {x3}")
    print(f"La resta de 7 y 8 es {x4}")